const express = require('express')
const fs = require('fs')
const path = require('path')
const os = require('os')
// const AWS = require('aws-sdk')

// const S3 = new AWS.S3({ region: 'us-east-1' })

const routes = express.Router()

routes.get('/', (req, res) => {
  res.send('Hello')
})

routes.post('/createFile', async (req, res) => {
  try {
    const { nome } = req.body

    if (nome) {
      const filePath = path.join(os.tmpdir(), `${nome}.txt`)

      if (fs.existsSync(filePath)) {
        fs.unlink(filePath)
      }

      fs.writeFile(filePath, nome)

      console.log(nome)
    }

    res.status(400).send('Bad request!')
  } catch (error) {
    res.status(500).json(error)
  }
})

module.exports = routes
